// 个人数据示例
let mineInfos = {
	data: [
		{
			type: 'image',
			label: '广场可见',
			address: '武汉市',
			description: '穷途末路',
			createTime: '2022-07-30 15:05:25',
			imgs: ['/static/txw.jpeg', '/static/txw.jpeg']
		},
		{
			type: 'text',
			label: '广场可见',
			address: '南京市',
			description: '我要找对象',
			createTime: '2022-06-29 14:03:49'
		},
		{
			type: 'image',
			label: '广场可见',
			address: '武汉市',
			description: '还有没睡的嘛',
			createTime: '2022-06-29 14:03:49',
			imgs: ['/static/txw.jpeg']
		},
		{
			createTime: '2022-06-29 14:03:49',
			label: '仅自己可见',
			type: 'image',
			address: '广州市',
			description: '人生无常，大肠换小肠!!!',
			imgs: ['/static/txn.jpeg']
		},
	]
}

function MineApi() {}

MineApi.prototype.getMines = function() {
	return mineInfos.data;
}

export default new MineApi();