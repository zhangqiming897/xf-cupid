// 广场数据示例
let findInfos = {
	tags: [
		{
			type: 'concern',
			title: '#不完美女孩'
		},
		{
			type: 'concern',
			title: '#失恋博物馆'
		},
		{
			type: 'concern',
			title: '#接受不完美的自己'
		}
	],
	data: [
		{
			type: 'concern',
			show: 'image',
			logo: '/static/images/Avatar-1.png',
			name: '王萌',
			label: '真希望你没见过什么世面，只爱我这张平凡的脸',
			imgs: ['/static/txn.jpeg', '/static/txw.jpeg'],
			uploadTime: '2022.06.15 13:20:10',
			address: '南京市',
			commentsNum: 280,
			lovesNum: 100
		},
		{
			type: 'concern',
			show: 'text',
			logo: '/static/images/Avatar-1.png',
			name: '李尔丹',
			label: '真的栓Q了，好难过啊!',
			uploadTime: '2022.06.15 13:20:10',
			address: '武汉市',
			commentsNum: 280,
			lovesNum: 100
		},
		{
			type: 'concern',
			show: 'image',
			logo: '/static/images/Avatar-1.png',
			name: '李木',
			label: '我的指甲也太piu亮啦!~',
			imgs: ['/static/txw.jpeg'],
			uploadTime: '2022.07.15 14:30:21',
			address: '深圳市',
			commentsNum: 280,
			lovesNum: 100
		},
		{
			type: 'recommend',
			show: 'text',
			logo: '/static/images/Avatar-1.png',
			name: '李卓',
			label: '面对人生的抉择时，真的很难选择!',
			uploadTime: '2022.06.15 13:20:10',
			address: '天津市',
			commentsNum: 280,
			lovesNum: 100
		},
		{
			type: 'recommend',
			show: 'image',
			logo: '/static/images/Avatar-1.png',
			name: '梦想家',
			label: '曾经有一个梦想，那就是梦想仗剑走天涯!',
			imgs: ['/static/txw.jpeg'],
			tags: ['#发自拍', '#分享心情'],
			uploadTime: '2022.08.20 15:30:30',
			address: '南京市',
			commentsNum: 32,
			lovesNum: 165
		},
		{
			type: 'recommend',
			show: 'image',
			logo: '/static/images/Avatar-1.png',
			name: '天涯何处无芳草',
			label: '今天正是一个元气满满的一天',
			imgs: ['/static/txw.jpeg', '/static/txw.jpeg', '/static/txw.jpeg'],
			tags: ['#美食', '#发自拍', '#心情愉悦'],
			uploadTime: '2022.09.10 13:30:25',
			address: '武汉市',
			commentsNum: 40,
			loveNum: 65
		}
	]
}

function FindApi() {}

FindApi.prototype.getTags = function () {
	return findInfos.tags;
}

FindApi.prototype.getRecommends = function () {
	let recommendList = findInfos.data.filter(v => v.type == 'recommend');
	return recommendList;
}

FindApi.prototype.getConcerns = function () {
	let concernList = findInfos.data.filter(v => v.type == 'concern');
	return concernList;
}

export default new FindApi();