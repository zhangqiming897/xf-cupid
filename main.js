import Vue from 'vue';
import App from './App';
import utils from 'common/util.js'
import uView from '@/uni_modules/uview-ui'
import GoEasy from 'goeasy';

Vue.config.productionTip = false;
App.mpType = 'app';

// 此处为演示Vue.prototype使用，非uView的功能部分
Vue.prototype.$appId = "appId";
Vue.prototype.$showToast = utils.showToast
Vue.prototype.$copyTxt = utils.copyTxt 


// 引入全局GoEasy
Vue.prototype.GoEasy = GoEasy;
Vue.prototype.goEasy = GoEasy.getInstance({
	host: "hangzhou.goeasy.io",
	appkey:"BC-85c1f648297b4b62a389b4230076f90f",
	modules:['im']
})
Vue.prototype.formatDate = function (t) {
    t = t || Date.now();
    let time = new Date(t);
    let str = time.getMonth() < 9 ? ('0' + (time.getMonth() + 1)) : (time.getMonth() + 1);
    str += '-';
    str += time.getDate() < 10 ? ('0' + time.getDate()) : time.getDate();
    str += ' ';
    str += time.getHours();
    str += ':';
    str += time.getMinutes() < 10 ? ('0' + time.getMinutes()) : time.getMinutes();
    return str;
};

// 引入全局uView
// Vue.use(uView);
const app = new Vue({
	...App
});

app.$mount();
